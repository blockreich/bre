package net.blockreich.blockreicheconomy;

import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.SQLException;

public final class BlockreichEconomy extends JavaPlugin {

    private static BlockreichEconomy plugin;
    private EconomyDatabase database;
    public ConfigReader reader;

    @Override
    public void onEnable() {
        plugin = this;
        getLogger().info("Aktviere BlockreichEconomy...");
        database = new EconomyDatabase(this, new File(getDataFolder(), "db"));
        if (!database.connect()) {
            getLogger().severe("Konnte Datenbank nicht öffen, deaktiviere....");
            getPluginLoader().disablePlugin(this);
        }
        reader = new ConfigReader(this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private static BlockreichEconomy getInstance() {
        return plugin;
    }

    /**
     * @param player Der Spieler, von dessem Account das Geld abgezogen wird.
     * @param amount Die Menge an Geld die dem Spieler abgezogen werden soll.
     * @return true, wenn erfolgreich.
     */
    public static boolean withdrawPlayer(OfflinePlayer player, double amount){
        double newBalance = getInstance().database.getBalance(player.getUniqueId())-amount;
        if(newBalance<0){
            return false;
        }
        getInstance().database.updateBalance(player.getUniqueId(), newBalance);
        return true;
    }

    public boolean payPlayer(OfflinePlayer player, double amount) {
        double newBalance = getInstance().database.getBalance(player.getUniqueId())+amount;
        if(getInstance().database.updateBalance(player.getUniqueId(), newBalance)) {
            return true;
        } else return false;
    }


    /**
     * Erzeugt ein Konto für einen Spieler.
     * @param player Der Spieler für den ein Konto erzeugt werden soll.
     * @return True, wenn das Konto erzeugt wurde, False, wenn der Spieler schon ein Konto hat, oder ein Fehler aufgetreten ist.
     */
    public static boolean createAccount(OfflinePlayer player) {
        try {
            if(getInstance().database.hasAccount(player.getUniqueId())) {
                return false;
            }
            getInstance().database.insertUserAccount(player.getUniqueId());
            return true;
        } catch (Exception e) {
           return false;
        }
    }

    /**
     * @param player Der Spieler, dessen Konto gesucht werden soll.
     * @return True, wenn der Spieler ein Konto besitzt, False wenn ein Fehler aufgetreten ist oder der User kein Konto besitzt.
     */
    public boolean hasAccount(OfflinePlayer player){
        try {
            return getInstance().database.hasAccount(player.getUniqueId());
        } catch (SQLException e) {
            return false;
        }
    }

    /**
     * @param victim Der Spieler, dem das Geld abgezogen werden soll.
     * @param target Der Spieler, dem der Betrag gutgeschrieben werden soll.
     * @param amount Der Betrag.
     * @return true, wenn erfolgreich, ansonsten false.
     */
    public boolean transact(OfflinePlayer victim, OfflinePlayer target, double amount) {
        if (!withdrawPlayer(victim, amount)) { return false; }
        return payPlayer(target, amount);
    }

    public double getBalance(OfflinePlayer p){
        return getInstance().database.getBalance(p.getUniqueId());
    }
    //API Functions
}
